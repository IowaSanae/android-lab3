import 'package:flutter/material.dart';
// import 'package:flutter_app_login_form_week4/bloc/bloc_stream.dart';
import '../stream/message_stream.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      home: const HomePage(title: 'Demo'),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required String title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  MessageStream messageStream = MessageStream();
  List<String> texts = [];

  HomePageState() {
    printMessage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView.builder(
          itemCount: texts.length,
          itemBuilder: (BuildContext buildContext, int index) {
            return Container(
                padding: const EdgeInsets.all(10), child: Text(texts[index]));
          }),
    );
  }

  // Prints messages on the screen.
  printMessage() async {
    messageStream.getMessages().listen((message) {
      setState(() {
        texts.add(message.toString());
      });
    });
  }
}
