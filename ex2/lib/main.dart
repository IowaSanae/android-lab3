import 'package:flutter/material.dart';
import 'stream/image_stream.dart' as MyImageStream;
import 'models/images.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  MyImageStream.ImageStream imageStream = MyImageStream.ImageStream();
  List<ImageModel> images = [];

  _MyHomePageState() {
    getStream();
  }

  getStream() async {
    imageStream.getImageModel().listen((image) {
      setState(() {
        images.add(image!);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: ListView.builder(
          itemCount: images.length,
          itemBuilder: (BuildContext buildContext, int index) {
            return Container(
                padding: const EdgeInsets.all(10),
                child: Image.network(images[index].url ?? ""));
          },
        ));
  }
}
