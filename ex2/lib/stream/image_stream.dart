import 'dart:convert';
import 'dart:math';

import '../models/images.dart';
import 'package:http/http.dart' as http;

class ImageStream {
  List<ImageModel> imageList = [];

  fetchImage() {
    Uri url = Uri.parse('https://jsonplaceholder.typicode.com/photos');
    http.get(url).then((response) {
      var jsonArray = json.decode(response.body);
      imageList = List<ImageModel>.from(
          jsonArray.map((jsonData) => ImageModel.fromJson(jsonData)));
    });
  }


  ImageStream() {
    fetchImage();
  }

  // Null-safety.
  Stream<ImageModel?> getImageModel() async* {
    const _duration = Duration(seconds: 5);
    int _random = Random().nextInt(100);
    ImageModel? _imageModel;
    yield* Stream.periodic(_duration, (int i) {
      if (imageList.isNotEmpty) {
        _imageModel = imageList[_random];
      }

      return _imageModel;
    });
  }
}
